import numpy as np
import matplotlib.pyplot as plt

@interact
def foo(count=(1,10,1), t=slider(100,1000,100,default=100), y=.2):

    plt.cla()         
    dt = 1/t
    
    x = np.arange(1, step=dt)
    xl = np.arange(1, step=.000001)
    
    lin = y + x*0
    plt.plot(x, lin)
    
    W = np.zeros(t, np.dtype(float))
       
    l = np.sqrt(2*xl*ln(ln(1/xl)))
    plt.plot(xl, l,'r--') # <= add subplot    
    plt.plot(xl, -l,'r--')  
        
    mu, sig = 0, 1
       
    for ITER in range(1, count+1):
        for i in range(1, len(W)):
            W[i] = W[i-1] + np.random.normal(mu, sig) * np.sqrt(dt)
        plt.plot(x,W)
        
        #print '\nlin ', lin
        #print '\nW ', W
        Func = lin - W
        Sign = np.sign(Func)  
        #print '\nSign ', Sign
        Diff = np.diff(Sign)
        #print '\nDiff ', Diff
        Points = np.where(Diff != 0)[0]        
        print '\nPoints of intersection =', Points*dt
        
    plt.xlabel('t',fontsize=26)
    plt.ylabel('W(t)',fontsize=26)
    plt.grid(True)
            
    @interact
    def _dee(deep=slider(0.0001, .4, 0.001, default=.2)):
        plt.xlim(0, deep)
        plt.ylim(-.5, .5)
        plt.show()









